'use strict';
const recurring_bedrooms = 20;
const one_off_bedrooms = 45;
const endofLease_bedrooms = 70;
const recurring_bathrooms = 40;
const one_off_bathrooms = 85;
const endofLease_bathrooms = 130;
const price_recurring = 33;
const price_one_off = 40;
const price_endofLease = 40;
const condition_slight = 0.75;
const condition_moderate = 1.0;
const condition_heavy = 1.55;
function calc_Time(bedrooms, bathrooms, condition, type) {
    var tbad, tbath, price;
    switch (type) {
        case "recurring":
            (tbad = recurring_bedrooms),
                (tbath = recurring_bathrooms),
                (price = price_recurring_bedrooms);
            break;
        case "one_off":
            tbad = one_off_bedrooms;
            (tbath = one_off_bathrooms), (price = price_one_off_bedrooms);
            break;
        case "endofLease":
            (tbad = endofLease_bedrooms),
                (tbath = endofLease_athrooms),
                (price = price_endofLease_bedrooms);
            break;
    }
    timeroom = tbad * bedrooms;
    timebath = tbath * bathrooms;
    totalTime =
        (bedrooms * timeroom * condition) / 100 +
        (bathrooms * timebath * condition) / 100;
    totalPrice = (totalTime / 60) * price;
    console.log(calc_Time);
    return calc_Time(bedrooms, bathrooms, condition, type);
}