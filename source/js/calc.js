"use strict";
const recurring_bedrooms = 20;
const one_off_bedrooms = 45;
const endofLease_bedrooms = 70;
const recurring_bathrooms = 40;
const one_off_bathrooms = 85;
const endofLease_bathrooms = 130;
const price_recurring = 33;
const price_one_off = 40;
const price_endofLease = 40;
const condition_slight = 75;
const condition_moderate = 100;
const condition_heavy = 155;

function calc() {
    var bedrooms = document.getElementsByName("bedrooms");
    for (var i = 0, length = bedrooms.length; i < length; i++) {
        if (bedrooms[i].checked) {
            bedrooms = bedrooms[i].value;
            break;
        }
    }
    var Bathrooms = document.getElementsByName("Bathrooms");
    for (var i = 0, length = Bathrooms.length; i < length; i++) {
        if (Bathrooms[i].checked) {
            Bathrooms = Bathrooms[i].value;
            break;
        }
    }
    var type = document.getElementsByName("clean_type");
    for (var i = 0, length = type.length; i < length; i++) {
        if (type[i].checked) {
            type = type[i].value;
            break;
        }
    }
    var condition = document.getElementsByName("condition");
    for (var i = 0, length = condition.length; i < length; i++) {
        if (condition[i].checked) {
            condition = condition[i].value;
            break;
        }
    }
    var tbad,
        tbath,
        price,
        timeroom,
        timebath,
        totalTime,
        totalPrice;
    switch (type) {
        case "recurring":
            (tbad = recurring_bedrooms),
                (tbath = recurring_bathrooms),
                (price = price_recurring);
            break;
        case "one_off":
            tbad = one_off_bedrooms;
            (tbath = one_off_bathrooms), (price = price_one_off);
            break;
        case "endofLease":
            (tbad = endofLease_bedrooms),
                (tbath = endofLease_bathrooms),
                (price = price_endofLease);
            break;
    }
    timeroom = tbad * bedrooms;
    timebath = tbath * Bathrooms;
    totalTime =
        (timeroom * condition / 100)  +
        (timebath * condition / 100);
    totalPrice = (totalTime / 60) * price;
    console.log(timeroom);
    document.getElementById("hours").innerText = totalTime;
    document.getElementById("price_clean").innerText = totalPrice;
}
