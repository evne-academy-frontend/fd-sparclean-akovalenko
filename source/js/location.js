"use strict";
function initMap() {
    var center = new google.maps.LatLng (-34.546019, 150.857020)
    var map = new google.maps.Map(document.getElementById("location_map"),{
        center: center,
        zoom: 8,
    });
    var image = {
        url:'./source/img/images/Shape.png',
        size: new google.maps.Size(30,40),
        scaledSize: new google.maps.Size(30,40)
    }
    var marker = new google.maps.Marker({
        position: center,
        map: map,
        icon:image,
      });
      var contentString = '<div id="content">' + 
      '<h3>Sparclean Services</h3>' + <p>Unit 5, 113 Industrial</p>
      <p><strong>Road Oak Flats NSW</strong></p>
      <p><strong>2529 PO BOX 351</strong></p>
      <p><strong>Albion Park, 2527</strong></p>
      <p><strong>0467 323 979</strong></p>
      <p><strong>0424 373 421</p>
      <p>hello@sparclean.com.au</p>
      <p>© 2016 Sparclean Services.</p>
}
